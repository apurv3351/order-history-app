class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.references :user, null: false, foreign_key: true
      t.string :product_code
      t.date :order_date
      t.string :user_email

      t.timestamps
    end
  end
end
