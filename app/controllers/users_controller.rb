class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def download_csv
    user_id = params[:id]
    CsvGenerationJob.perform_later(user_id)
    render json: { message: 'CSV generation started successfully' }
  end
end