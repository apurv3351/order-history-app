class CsvGenerationJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    user = User.find(user_id)
    orders = user.orders
    # Logic to generate CSV for user's order history
    # Write to file and save it in the 'download_order_csv' folder
  end
end
