import React from "react";

const UsersList = ({ users }) => {
  return (
    <div>
      <h2>Users</h2>
      <ul>
        {users.map((user) => (
          <li key={user.id}>
            {user.name}
            <button onClick={() => downloadCsv(user.id)}>Download CSV</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

const downloadCsv = (userId) => {
  console.log("Downloading CSV for user with id:", userId);
  fetch(`/users/${userId}/export`, {
    method: "GET",
    headers: {
      "Content-Type": "text/csv",
    },
  })
    .then((response) => {
      response.blob().then((blob) => {
        const url = window.URL.createObjectURL(new Blob([blob]));
        const link = document.createElement("a");
        console.log("Link:", link);
        link.href = url;
        link.setAttribute("download", "user.csv");
        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
      });
    })
    .catch((error) => {
      console.error("Error:", error);
    });
};

export default UsersList;
