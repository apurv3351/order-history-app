require 'csv'

users_csv = CSV.read('input_csv/users.csv', headers: true)
users_csv.each do |row|
  User.create(name: row['name'], email: row['email'])
end

orders_csv = CSV.read('input_csv/orders.csv', headers: true)
orders_csv.each do |row|
  Order.create(user_id: row['user_id'], product_code: row['product_code'], purchase_date: row['purchase_date'])
end
