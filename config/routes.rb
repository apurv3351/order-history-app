Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  resources :users, only: [:index] do
    member do
      post 'download_csv'
    end
  end
end
